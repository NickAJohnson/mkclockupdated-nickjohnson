﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SaveLoadManager : MonoBehaviour
{

    public static SaveLoadManager Instance;
    [SerializeField] protected ClocksUI clockHolder;
    [Tooltip("For Testing")]
    [SerializeField] protected bool clearDataOnStart = false;

    [SerializeField] protected float saveTimerAmount;
    private float timer;

    private Globals globals;

    private void Awake()
    {
        Instance = this;

        if (clearDataOnStart)
            ClearSavedData();
    }

    private void Start()
    {
        globals = Globals.Instance;
        if(!clearDataOnStart)
            LoadAllSavedClocks();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            SaveAllClocks();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadAllSavedClocks();
        }

        //Regular Save
        timer -= Time.deltaTime;
        if(timer < 0)
        {
            timer = saveTimerAmount;
            SaveAllClocks();
        }
    }

    private void OnApplicationQuit()
    {
        SaveAllClocks();
    }

    public void SaveAllClocks()
    {
        SaveClockList(clockHolder.DisplayClocks.ToList<Clock>());
        SaveClockList(clockHolder.TimerClocks.ToList<Clock>());
        SaveClockList(clockHolder.StopwatchClocks.ToList<Clock>());
    }

    public void SaveClockList(List<Clock> clockList)
    {
        if (clockList.Count <= 0)
            return;

        string code = clockList[0].GetSaveTag();

        //Used for loading amount
        PlayerPrefs.SetInt(code + "Count", clockList.Count);

        for (int i = 0; i < clockList.Count; i++)
        {
            SaveClock(code + i.ToString(), clockList[i]);
        }
    }

    private void SaveClock(string clockCode, Clock clock)
    {
        //A Clock Saves: Time, Format 
        PlayerPrefs.SetFloat(clockCode + globals.hoursTag, clock.CurrentTime.x);   
        PlayerPrefs.SetFloat(clockCode + globals.minutesTag, clock.CurrentTime.y);   
        PlayerPrefs.SetFloat(clockCode + globals.secondsTag, clock.CurrentTime.z);

        PlayerPrefs.SetInt(clockCode + globals.formatTag, (int)clock.Format);
    }

    public void LoadAllSavedClocks()
    {
        string dispCode = Globals.Instance.displayClockTag;
        string timerCode = Globals.Instance.timerClockTag;
        string stopwatchCode = Globals.Instance.stopwatchClockTag;

        if(PlayerPrefs.HasKey(dispCode + globals.countTag) && PlayerPrefs.HasKey(dispCode + globals.countTag) && PlayerPrefs.HasKey(dispCode + globals.countTag))
        {
            clockHolder.ClearAllClocks();
        }
        else
        {
            Debug.Log("Attempting Load Without Save");
            return;
        }

        for (int i = 0; i < PlayerPrefs.GetInt(dispCode + globals.countTag); i++)
        {
            clockHolder.AddDisplayClock(LoadDisplayClock(dispCode + i.ToString()));
        }
        for (int i = 0; i < PlayerPrefs.GetInt(timerCode + globals.countTag); i++)
        {
            clockHolder.AddTimerClock(LoadTimerClock(timerCode + i.ToString()));
        }
        for (int i = 0; i < PlayerPrefs.GetInt(stopwatchCode + globals.countTag); i++)
        {
            clockHolder.AddStopwatchClock(LoadStopwatchClock(stopwatchCode + i.ToString()));
        }
    }

    //Could not combine these with type casting
    private DisplayClock LoadDisplayClock(string clockCode)
    {
        Vector3 loadedTime = LoadPlayerPrefTime(clockCode);

        DisplayClock spawnedClock = ObjectPooler.Instance.SpawnFromPool("Display", transform.position, Quaternion.identity).GetComponent<DisplayClock>();
        spawnedClock.SetTime(loadedTime);
        spawnedClock.Format = (Clock.ClockFormat)PlayerPrefs.GetInt(clockCode + globals.formatTag);

        return spawnedClock;
    }

    private TimerClock LoadTimerClock(string clockCode)
    {
        Vector3 loadedTime = LoadPlayerPrefTime(clockCode);

        TimerClock spawnedClock = ObjectPooler.Instance.SpawnFromPool("Timer", transform.position, Quaternion.identity).GetComponent<TimerClock>();
        spawnedClock.SetTime(loadedTime);
        spawnedClock.Format = (Clock.ClockFormat)PlayerPrefs.GetInt(clockCode + globals.formatTag);

        return spawnedClock;
    }

    private StopwatchClock LoadStopwatchClock(string clockCode)
    {
        Vector3 loadedTime = LoadPlayerPrefTime(clockCode);

        StopwatchClock spawnedClock = ObjectPooler.Instance.SpawnFromPool("Stopwatch", transform.position, Quaternion.identity).GetComponent<StopwatchClock>();
        spawnedClock.SetTime(loadedTime);
        spawnedClock.Format = (Clock.ClockFormat)PlayerPrefs.GetInt(clockCode + globals.formatTag);

        return spawnedClock;
    }

    private Vector3 LoadPlayerPrefTime(string code)
    {
        return new Vector3
            (
            PlayerPrefs.GetFloat(code + globals.hoursTag),
            PlayerPrefs.GetFloat(code + globals.minutesTag),
            PlayerPrefs.GetFloat(code + globals.secondsTag)
            );
    }

    public void ClearSavedData()
    {
        PlayerPrefs.DeleteAll();
    }
}

