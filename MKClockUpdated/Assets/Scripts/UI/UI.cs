﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI : MonoBehaviour
{
    [HideInInspector]
    public UI previousUI;
    protected UIManager manager;



    //Allows a UI to hold a function that can be executed by a ConfirmationMenuUI
    public delegate void StoreFunction();
    [HideInInspector]
    public StoreFunction storedFunction;

    protected void Awake()
    {
        manager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
    }

    abstract public void ActiveUIUpdate();

    abstract public void OnActivated();

    public void PreviousUI()
    {
        manager.GoBackUI();
    }
}
