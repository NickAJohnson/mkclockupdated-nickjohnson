﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Allows User to Edit the Time of a Clock
public class TimeSetUI : UI
{
    private bool isDirty = true;
    protected Vector3 newTime;
    protected Clock givenClock;

    [Tooltip("Left to Right")]
    [SerializeField] protected List<Image> digitImages;

    public Clock GivenClock
    {
        get
        {
            return givenClock;
        }
        set
        {
            givenClock = value;
            newTime = givenClock.CurrentTime;
            isDirty = true;
        }
    }


    public override void ActiveUIUpdate()
    {
        if (isDirty)
        {
            Globals.Instance.SetDigits(digitImages, newTime, true);
            isDirty = false;
        }
    }

    public override void OnActivated()
    {
        newTime = Vector3.zero;
    }

    #region ClockFunctions

    protected void AddSeconds(float amount)
    {

        newTime.z += amount;
        isDirty = true;
    }

    protected void AddMinutes(float amount)
    {
        newTime.y += amount;
        isDirty = true;

    }

    protected void AddHours(float amount)
    {
        newTime.x += amount;
        isDirty = true;
    }

    #endregion

    #region Buttons
    public void HourUp()
    {
        newTime.x++;
        isDirty = true;
    }
    public void HourDown()
    {
        newTime.x--;
        isDirty = true;
    }
    public void MinuteUp()
    {
        newTime.y++;
        isDirty = true;
    }
    public void MinuteDown()
    {
        newTime.y--;
        isDirty = true;
    }
    public void SecondUp()
    {
        newTime.z++;
        isDirty = true;
    }
    public void SecondDown()
    {
        newTime.z--;
        isDirty = true;
    }


    public void ConfirmTime()
    {
        givenClock.SetTime(newTime);
        UIManager.Instance.GoBackUI();
    }

    public void Cancel()
    {
        UIManager.Instance.GoBackUI();
    }
    #endregion

}
