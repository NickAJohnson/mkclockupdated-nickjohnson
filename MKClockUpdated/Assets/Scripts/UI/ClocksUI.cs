﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClocksUI : UI
{
    [Header("Settings")]
    [SerializeField] protected int clockCap = 10;
    [SerializeField] protected int clockMinimum = 1;

    [Header("RequiredFields")]
    [SerializeField] protected GridLayoutGroup displayGridLayout;
    [SerializeField] protected GridLayoutGroup timerGridLayout;
    [SerializeField] protected GridLayoutGroup stopwatchGridLayout;

    private RectTransform displayGridRT;
    private RectTransform timerGridRT;
    private RectTransform stopwatchGridRT;

    [SerializeField] protected CanvasGroup displayClocksCG;
    [SerializeField] protected CanvasGroup timerClocksCG;
    [SerializeField] protected CanvasGroup stopwatchClocksCG;

    public delegate void AddDisplayClockDel(DisplayClock clock);
    public AddDisplayClockDel displayDel;
    public delegate void AddTimerClockDel(TimerClock clock);
    public AddTimerClockDel timerDel;
    public delegate void AddStopwatchClockDel(StopwatchClock clock);
    public AddStopwatchClockDel stopwatchDel;

    protected List<DisplayClock> displayClocks = new List<DisplayClock>();
    public List<DisplayClock> DisplayClocks
    {
        get { return displayClocks; }
        private set { }
    }

    protected List<TimerClock> timerClocks = new List<TimerClock>();
    public List<TimerClock> TimerClocks
    {
        get { return timerClocks; }
        private set { }
    }

    protected List<StopwatchClock> stopwatchClocks = new List<StopwatchClock>();
    public List<StopwatchClock> StopwatchClocks
    {
        get { return stopwatchClocks; }
        private set { }
    }

    private ObjectPooler objPooler;


    public override void ActiveUIUpdate()
    {
    }

    public override void OnActivated()
    {
    }

    protected void Awake()
    {
        displayGridRT = displayGridLayout.GetComponent<RectTransform>();
        timerGridRT = timerGridLayout.GetComponent<RectTransform>();
        stopwatchGridRT = stopwatchGridLayout.GetComponent<RectTransform>();

        //Screen width minus scroller width
        displayGridLayout.cellSize = new Vector2(Screen.width - 20, Screen.width - 20); 
        timerGridLayout.cellSize = new Vector2(Screen.width - 20, Screen.width - 20); 
        stopwatchGridLayout.cellSize = new Vector2(Screen.width - 20, Screen.width - 20);

        displayDel = AddDisplayClock;
        timerDel = AddTimerClock;
        stopwatchDel = AddStopwatchClock;
        base.Awake();
    }

    private void Start()
    {
        objPooler = ObjectPooler.Instance;

        //Ensure minimum clocks reached
        for(int i = 0; i < clockMinimum - displayClocks.Count; i++)
            AddDisplayClock();
        for (int i = 0; i < clockMinimum - timerClocks.Count; i++)
            AddTimerClock();
        for (int i = 0; i < clockMinimum - stopwatchClocks.Count; i++)
            AddStopwatchClock();

        SetViewDisplayClocks();
    }

    private void SetView(CanvasGroup activeClocks)
    {
        ToggleCanvasGroup(displayClocksCG, false);   
        ToggleCanvasGroup(timerClocksCG, false);   
        ToggleCanvasGroup(stopwatchClocksCG, false);

        ToggleCanvasGroup(activeClocks, true);
    }

    private void ToggleCanvasGroup(CanvasGroup target, bool state)
    {
        target.alpha = state ? 1 : 0;
        target.interactable = state;
        target.blocksRaycasts = state;
    }

    public void ChangeClockType(Clock clock, System.Type newType)
    {
        Vector3 timeStore = clock.CurrentTime;
        DeactivateClock(clock);
        if (newType == typeof(DisplayClock))
        {
            AddDisplayClock();
        }
        else if (newType == typeof(TimerClock))
        {
            AddTimerClock();
        }
        else if (newType == typeof(StopwatchClock))
        {
            AddStopwatchClock();
        }

    }

    public void DeactivateClock(Clock removedClock)
    {
        //Find Type, Check List, Remove Clock
        System.Type currentType = removedClock.GetType();
        if (currentType == typeof(DisplayClock))
        {
            DisplayClock clock = removedClock.GetComponent<DisplayClock>();
            if (clock)
            {
                if(displayClocks.Count > clockMinimum)
                {
                    displayClocks.Remove(clock);
                }else
                {
                    return;
                }
            }
        }
        else if (currentType == typeof(TimerClock))
        {
            TimerClock clock = removedClock.GetComponent<TimerClock>();
            if (clock)
            {
                if(timerClocks.Count > clockMinimum)
                {
                    timerClocks.Remove(clock);
                }
                else
                {
                    return;
                }
            }
        }
        else if (currentType == typeof(StopwatchClock))
        {
            StopwatchClock clock = removedClock.GetComponent<StopwatchClock>();
            if (clock)
            {
                if (stopwatchClocks.Count > clockMinimum)
                {
                    stopwatchClocks.Remove(clock);
                }
                else
                {
                    return;
                }
            }
        }
        removedClock.gameObject.SetActive(false);
    }

    #region Buttons

    public void SetViewDisplayClocks()
    {
        SetView(displayClocksCG);
    }

    public void SetViewTimerClocks()
    {
        SetView(timerClocksCG);
    }

    public void SetViewStopwatchClocks()
    {
        SetView(stopwatchClocksCG);
    }

    public void AddDisplayClock()
    {
        if (displayClocks.Count < clockCap)
        {
            DisplayClock spawnedClock = objPooler.SpawnFromPool("Display", transform.position, Quaternion.identity).GetComponent<DisplayClock>();
            if (spawnedClock == null)
            {
                Debug.LogError("Invalid Object Pool Name");
                return;
            }
            AddDisplayClock(spawnedClock);
        }
    }

    public void AddDisplayClock(DisplayClock clock)
    {
        if(displayClocks.Count < clockCap)
        {
            displayClocks.Add(clock);
            clock.transform.SetParent(displayGridLayout.transform);
        }
    }

    public void RemoveDisplayClock()
    {
        if (displayClocks.Count > clockMinimum && displayClocks.Count > 0)
        {
            DisplayClock removedClock = displayClocks[displayClocks.Count - 1];
            displayClocks.Remove(removedClock);
            removedClock.gameObject.SetActive(false);
        }
    }

    public void AddTimerClock()
    {
        if (timerClocks.Count < clockCap)
        {
            TimerClock spawnedClock = objPooler.SpawnFromPool("Timer", transform.position, Quaternion.identity).GetComponent<TimerClock>();
            if (spawnedClock == null)
            {
                Debug.LogError("Invalid Object Pool Name");
                return;
            }
            AddTimerClock(spawnedClock);
            timerGridRT.sizeDelta = new Vector2(timerGridLayout.cellSize.x, (float)timerClocks.Count * timerGridLayout.cellSize.y);

        }
    }

    public void AddTimerClock(TimerClock clock)
    {
        if (timerClocks.Count < clockCap)
        {
            timerClocks.Add(clock);
            clock.transform.SetParent(timerGridLayout.transform);
        }
    }

    public void RemoveTimerClock()
    {
        if (timerClocks.Count > clockMinimum && timerClocks.Count > 0)
        {
            TimerClock removedClock = timerClocks[timerClocks.Count - 1];
            timerClocks.Remove(removedClock);
            removedClock.gameObject.SetActive(false);

            timerGridRT.sizeDelta = new Vector2(timerGridLayout.cellSize.x, (float)timerClocks.Count * timerGridLayout.cellSize.y);
        }
    }

    public void AddStopwatchClock()
    {
        if (stopwatchClocks.Count < clockCap)
        {
            StopwatchClock spawnedClock = objPooler.SpawnFromPool("Stopwatch", transform.position, Quaternion.identity).GetComponent<StopwatchClock>();
            if (spawnedClock == null)
            {
                Debug.LogError("Invalid Object Pool Name");
                return;
            }

            AddStopwatchClock(spawnedClock);

            stopwatchGridRT.sizeDelta = new Vector2(stopwatchGridLayout.cellSize.x, (float)stopwatchClocks.Count * stopwatchGridLayout.cellSize.y);
        }
    }

    public void AddStopwatchClock(StopwatchClock clock)
    {
        if (stopwatchClocks.Count < clockCap)
        {
            stopwatchClocks.Add(clock);
            clock.transform.SetParent(stopwatchGridLayout.transform);
        }
    }

    public void RemoveStopwatchClock()
    {
        if (stopwatchClocks.Count > clockMinimum && stopwatchClocks.Count > 0)
        {
            StopwatchClock removedClock = stopwatchClocks[stopwatchClocks.Count - 1];
            stopwatchClocks.Remove(removedClock);
            removedClock.gameObject.SetActive(false);

            stopwatchGridRT.sizeDelta = new Vector2(stopwatchGridLayout.cellSize.x, (float)stopwatchClocks.Count * stopwatchGridLayout.cellSize.y);
        }
    }

    public void ClearAllClocks()
    {
        int temp = clockMinimum;
        clockMinimum = 0;
        for(int i = displayClocks.Count; i > 0; i--)
        {
            RemoveDisplayClock();
        }
        for (int i = timerClocks.Count; i > 0; i--)
        {
            RemoveTimerClock();
        }
        for (int i = stopwatchClocks.Count; i > 0; i--)
        {
            RemoveStopwatchClock();
        }
        clockMinimum = temp;
    }

    #endregion
}
