﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerAlert : UI
{
    [SerializeField] protected ClocksUI clocks;
    public override void ActiveUIUpdate()
    {
    }

    public override void OnActivated()
    {

    }

    public void DisarmAlarms()
    {
        foreach(TimerClock tc in clocks.TimerClocks)
        {
            tc.DeactivateAlarm();
        }
        UIManager.Instance.GoBackUI();
    }

}
