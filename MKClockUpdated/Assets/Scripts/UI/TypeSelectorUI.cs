﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeSelectorUI : UI
{
    [SerializeField] protected ClocksUI clocks;
    [HideInInspector] public Clock givenClock;
    public override void ActiveUIUpdate()
    {
    }

    public override void OnActivated()
    {
    }

    public void OnDisplayPress()
    {
        if(givenClock.GetType() != typeof(DisplayClock))
        {
            clocks.ChangeClockType(givenClock, typeof(DisplayClock));
        }
        UIManager.Instance.GoBackUI();
    }
    public void OnTimerPress()
    {
        if (givenClock.GetType() != typeof(TimerClock))
        {
            clocks.ChangeClockType(givenClock, typeof(TimerClock));
        }
        UIManager.Instance.GoBackUI();
    }
    public void OnStopwatchPress()
    {
        if (givenClock.GetType() != typeof(StopwatchClock))
        {
            clocks.ChangeClockType(givenClock, typeof(StopwatchClock));
        }
        UIManager.Instance.GoBackUI();
    }
    public void OnCancelPress()
    {
        UIManager.Instance.GoBackUI();
    }
}
