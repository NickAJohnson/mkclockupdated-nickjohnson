﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Manager Class for game UIs

//The order UIs should be listed in

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    public List<UI> uiHistory;
    public UI activeUI;
    [Tooltip("This must be correctly ordered")]
    public List<UI> uiList;

    private void Awake()
    {
        Instance = this;
        uiHistory = new List<UI>();
    }

    void Start()
    {
        foreach(UI ui in uiList)
        {
            ui.gameObject.SetActive(false);
        }
        ActivateNewUI(activeUI);
    }

    void Update()
    {
        activeUI.ActiveUIUpdate();
    }


    //Replace ActiveUI with given UI
    public void ActivateNewUI(UI newActiveUI)
    {
        uiHistory.Add(activeUI);
        foreach (UI ui in uiList)
        {
            ui.gameObject.SetActive(false);
        }

        ActivateUI(newActiveUI);

        //HUD is the base UI
        if (newActiveUI == uiList[0])
        {
            uiHistory.Clear();
        }
    }

    //Activate new UI but have previous UI visible
    public void ActivateOnTopOfUI(UI newActiveUI)
    {
        //Set Previous UI for return buttons
        uiHistory.Add(activeUI);
        ActivateUI(newActiveUI);

        //HUD is the base UI
        if (newActiveUI == uiList[0])
        {
            uiHistory.Clear();
        }
    }

    //Swap active UI to stored previousUI
    public void GoBackUI()
    {
        activeUI.gameObject.SetActive(false);
        activeUI = uiHistory[uiHistory.Count - 1];
        uiHistory.RemoveAt(uiHistory.Count - 1);
        activeUI.gameObject.SetActive(true);
    }

    private void ActivateUI(UI newActiveUI)
    {
        activeUI = newActiveUI;
        activeUI.gameObject.SetActive(true);
        activeUI.OnActivated();
    }

}
