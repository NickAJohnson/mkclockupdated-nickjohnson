﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopwatchClock : UserControlledClock
{
    // Update is called once per frame
    protected override void Update()
    {
        if(running)
        {
            naturalTime += Time.deltaTime;
            if (naturalTime > 1)
            {
                naturalTime -= 1;
                AddSeconds(1);
            }

            if (isDirty)
            {
                UpdateValues();
            }
        }
    }

    public override string GetSaveTag()
    {
        return Globals.Instance.stopwatchClockTag;
    }

    #region Buttons

    protected override void FullReset()
    {
        base.FullReset();
        Running = false;
    }


    #endregion
}
