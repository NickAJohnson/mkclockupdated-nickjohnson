﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Increases with time and displays on clockface
public class DisplayClock : Clock
{
    protected override void Update()
    {
        naturalTime += Time.deltaTime;
        if (naturalTime > 1)
        {
            naturalTime -= 1;
            AddSeconds(1);
        }

        if (isDirty)
        {
            UpdateValues();
        }
    }

    public override string GetSaveTag()
    {
        return Globals.Instance.displayClockTag;
    }

    #region Buttons


    #endregion

}
