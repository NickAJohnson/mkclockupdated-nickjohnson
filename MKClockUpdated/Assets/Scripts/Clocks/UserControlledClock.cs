﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Provides Play/Pause/Restart controls for clocks with more user interactions
public class UserControlledClock : Clock
{

    [SerializeField] protected Image pausePlayButtonAnalog;
    [SerializeField] protected Image pausePlayButtonDigital;
    [SerializeField] protected Sprite playSprite;
    [SerializeField] protected Sprite pauseSprite;

    protected bool running = false;
    protected bool Running
    {
        get
        {
            return running;
        }
        set
        {
            running = value;
            pausePlayButtonAnalog.sprite = running ? pauseSprite : playSprite;
            pausePlayButtonDigital.sprite = running ? pauseSprite : playSprite;

        }
    }

    public void ToggleRunning()
    {
        Running = !Running;
    }

    public virtual void RestartTimerButton()
    {
        FullReset();
    }
}
