﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerClock : UserControlledClock
{

    private bool hadTime = false;
    [Header("Audio Source")]
    [SerializeField] AudioSource alarm;


    protected override void Update()
    {
        if(Running)
        {
            if(HasTime())
            {
                naturalTime += Time.deltaTime;
                if (naturalTime > 1)
                {
                    naturalTime -= 1;
                    AddSeconds(-1);
                }

                if (isDirty)
                {
                    UpdateValues();
                }
            }
            else if(hadTime)
            {
                ActivateAlarm();
            }
        }
    }

    public override string GetSaveTag()
    {
        return Globals.Instance.timerClockTag;
    }

    private bool HasTime()
    {
        if(currentTime.x > 0 || currentTime.y > 0 || currentTime.z > 0)
        {
            hadTime = true;
            return true;
        }
        return false;
    }

    private void ActivateAlarm()
    {
        hadTime = false;
        alarm.Play();
        UIManager.Instance.ActivateOnTopOfUI(UIManager.Instance.uiList[2]);
    }

    public void DeactivateAlarm()
    {
        alarm.Stop();
    }

    public override void RestartTimerButton()
    {
        hadTime = false;

        Vector3 timeAtReset = currentTime;
        //Reset Time to Zero
        base.RestartTimerButton();
        //Reseting Timer without time passing will leave the timer at 0
        if (timeAtReset != lastSetTime && timeAtReset != Vector3.zero)
        {
            SetTime(lastSetTime);
        }
        Running = false;
    }


}
