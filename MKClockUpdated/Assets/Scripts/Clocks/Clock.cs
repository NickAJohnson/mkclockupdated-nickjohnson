﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Base Class of a clock - Has all functionality for tracking time
public class Clock : MonoBehaviour, iPooledObject
{

    //Enum provides further formats if adding timezones etc.
    public enum ClockFormat
    {
        ANALOG = 0,
        DIGITAL24,
        DIGITAL12
    }

    [Tooltip("hours, minutes, seconds")]
    protected Vector3 currentTime;
    public Vector3 CurrentTime
    {
        get { return currentTime; }
        protected set { currentTime = value; }
    }

    [SerializeField] protected ClockFormat format;
    public ClockFormat Format
    {
        get { return format; }
        set
        { 
            format = value; 
            if(format == ClockFormat.ANALOG)
            {
                ActivateAnalog();
            }
            else if(format == ClockFormat.DIGITAL24)
            {
                ActivateDigital(true);
            }
            else
            {
                ActivateDigital(false);
            }
        }
    }

    [Header("Analog")]
    //For Analog
    [SerializeField] GameObject analogHolder;
    [SerializeField]protected Transform secondHand;
    [SerializeField]protected Transform minuteHand;
    [SerializeField]protected Transform hourHand;

    [Header("Digital")]
    [SerializeField] GameObject digitalHolder;
    [Tooltip("0 - 9")]
    [SerializeField]protected List<Sprite> digitSprites;
    [Tooltip("Left to Right")]
    [SerializeField]protected List<Image> digitImages;

    [Header("Digital12")]
    [SerializeField] protected Image digitA;
    [SerializeField] protected Image digitP;
    [SerializeField] protected Image digitM;

    //Seperate float to add per second 
    protected float naturalTime;
    //Stores previous set time used by specific clocks
    protected Vector3 lastSetTime = Vector3.zero;

    //if Seconds, minutes or hours added, checks if conversion required
    protected bool isDirty = true;

    //UIs
    private TimeSetUI timeSetUI;
    private TypeSelectorUI typeSelectorUI;
    private ClocksUI clocks;


    private void Awake()
    {
        timeSetUI = GameObject.FindGameObjectWithTag("TimeSetUI").GetComponent<TimeSetUI>();
        typeSelectorUI = GameObject.FindGameObjectWithTag("TypeSelectorUI").GetComponent<TypeSelectorUI>();
        clocks = GameObject.FindGameObjectWithTag("ClocksUI").GetComponent<ClocksUI>();
    }
    // Start is called before the first frame update
    protected void Start()
    {

    }

    protected virtual void FullReset()
    {
        //Change to activate 
        currentTime.x = 0;
        currentTime.y = 0;
        currentTime.z = 0;
        naturalTime = 0;
        UpdateValues();
    }

    public void SetTime(Vector3 newTime)
    {
        currentTime = newTime;
        lastSetTime = newTime;
        isDirty = true;
        UpdateValues();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //Update Functions will Vary Per Clock
    }

    public virtual string GetSaveTag()
    {
        Debug.LogError("Save override not implemented");
        return null;
        //Override per class to seperate each clock class
    }


    protected void AddSeconds(float amount)
    {
        currentTime.z += amount;
        isDirty = true;
    }

    protected void AddMinutes(float amount)
    {
        currentTime.y += amount;
        isDirty = true;

    }

    protected void AddHours(float amount)
    {
        currentTime.x += amount;
        isDirty = true;
    }

    //Check conversions, update clock hands
    protected void UpdateValues()
    {
        currentTime = Globals.Instance.ApplyTimeBounds(currentTime);

        switch (format)
        {
            case ClockFormat.ANALOG:
                SetHandPositions();
                break;
            case ClockFormat.DIGITAL24:
                Globals.Instance.SetDigits(digitImages, currentTime, true);
                break;
            case ClockFormat.DIGITAL12:
                SetIsPM(Globals.Instance.SetDigits(digitImages, currentTime, false));
                break;
            default:
                break;
        }

        isDirty = false;
    }
    //Sets clock to pm if true, am if false
    protected void SetIsPM(bool isPM)
    {
        if(isPM)
        {
            digitA.gameObject.SetActive(false);
            digitP.gameObject.SetActive(true);
        }
        else
        {
            digitP.gameObject.SetActive(false);
            digitA.gameObject.SetActive(true);
        }
    }

    protected void SetHandPositions()
    {
        if (format != ClockFormat.ANALOG)
            return;

        secondHand.transform.rotation = Quaternion.Euler(Vector3.back * 360f * (currentTime.z / 60f));
        minuteHand.transform.rotation = Quaternion.Euler(Vector3.back * 360f * (currentTime.y / 60f));
        //hours/hours per day
        hourHand.transform.rotation = Quaternion.Euler(Vector3.back * 360f * (currentTime.x + currentTime.y/60f) / 12f);
    }

    #region ButtonFunctions
    public void ActivateAnalog()
    {
        format = ClockFormat.ANALOG;

        analogHolder.SetActive(true);
        digitalHolder.SetActive(false);
        isDirty = true;
    }

    public void ActivateDigital(bool is24Hour)
    {
        if(is24Hour)
        {
            format = ClockFormat.DIGITAL24;
        }
        else
        {
            format = ClockFormat.DIGITAL12;
        }

        digitA.gameObject.SetActive(!is24Hour);
        digitP.gameObject.SetActive(!is24Hour);
        digitM.gameObject.SetActive(!is24Hour);

        analogHolder.SetActive(false);
        digitalHolder.SetActive(true);
        isDirty = true;
    }

    public void Toggle24Hour()
    {
        if (format == ClockFormat.DIGITAL12)
        {
            ActivateDigital(true);
        }
        else
        {
            ActivateDigital(false);
        }
        
    }

    public void OpenSetTime()
    {
        //Activate Time Set UI;
        UIManager.Instance.ActivateOnTopOfUI(timeSetUI);
        timeSetUI.GivenClock = this;
    }

    public void OpenTimeFormat()
    {
        UIManager.Instance.ActivateOnTopOfUI(typeSelectorUI);
        typeSelectorUI.givenClock = this;
    }

    public void DeactivateClock()
    {
        clocks.DeactivateClock(this);
    }
    #endregion


    public void OnObjectSpawn()
    {
        FullReset();
    }

    public void OnObjectInstantiation()
    {
        //Switch statement for later additional formats
        switch (format)
        {
            case ClockFormat.ANALOG:
                ActivateAnalog();
                break;
            case ClockFormat.DIGITAL24:
                ActivateDigital(true);
                break;
            case ClockFormat.DIGITAL12:
                ActivateDigital(false);
                break;
            default:
                break;
        }
    }
}
