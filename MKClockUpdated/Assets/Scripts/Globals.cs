﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Globals : MonoBehaviour
{
    //[HideInInspector]
    public static Globals Instance;

    [Header("Time Settings")]
    //Values must be above 0 to prevent infinite loop
    [SerializeField][Range(1, 99)] protected int secondsPerMinute = 60;
    [SerializeField][Range(1, 99)] protected int minutesPerHour = 60;
    [SerializeField][Range(1, 99)] protected int hoursPerDay = 24;

    [Header("Digital Digits")]
    [Tooltip("0 - 9")]
    [SerializeField] protected List<Sprite> digitSprites;

    [Header("Save/Load Tags")]
    public string displayClockTag = "Display";
    public string timerClockTag = "Timer";
    public string stopwatchClockTag = "Stopwatch";

    public string countTag = "Count";
    public string hoursTag = "Hours";
    public string minutesTag = "Minutes";
    public string secondsTag = "Seconds";
    public string formatTag = "Format";

    private void Awake()
    {
        Instance = this;
    }

    public bool SetDigits(List<Image> digitsToSet, Vector3 newTime, bool is24Hour)
    {
        newTime = ApplyTimeBounds(newTime);
        bool isPM = false;
        if(!is24Hour)
        {
            isPM = CheckPM(ref newTime);
        }

        if(digitsToSet.Count != 6)
        {
            Debug.LogError("Invalid Digit Count - Globals/SetDigits");
            return false;
        }

        digitsToSet[0].sprite = digitSprites[(int)newTime.x / 10];
        digitsToSet[1].sprite = digitSprites[(int)newTime.x % 10];
        digitsToSet[2].sprite = digitSprites[(int)newTime.y / 10];
        digitsToSet[3].sprite = digitSprites[(int)newTime.y % 10];
        digitsToSet[4].sprite = digitSprites[(int)newTime.z / 10];
        digitsToSet[5].sprite = digitSprites[(int)newTime.z % 10];

        return isPM;
    }

    //Adjust time and returns true if pm
    protected bool CheckPM(ref Vector3 time)
    {
        if(time.x > hoursPerDay/2)
        {
            time.x -= hoursPerDay / 2;
            return true;
        }
        return false;
    }

    public Vector3 ApplyTimeBounds(Vector3 givenTime)
    {
        //Make sure all times are possitive for 2nd part of calculation;
        while(givenTime.z < 0)
        {
            givenTime.z += secondsPerMinute;
            givenTime.y--;
        }
        while (givenTime.y < 0)
        {
            givenTime.y += minutesPerHour;
            givenTime.x--;
        }
        while (givenTime.x < 0)
        {
            givenTime.x += hoursPerDay;
        }

        //Add minutes accumulated then minus seconds
        givenTime.y += (int)givenTime.z / secondsPerMinute;
        givenTime.z = givenTime.z % secondsPerMinute;

        givenTime.x += (int)givenTime.y / minutesPerHour;
        givenTime.y = givenTime.y % minutesPerHour;
        //givenTime.x += (int)givenTime.y / secondsPerMinute;

        givenTime.x = givenTime.x % hoursPerDay;

        return givenTime;
    }
}
