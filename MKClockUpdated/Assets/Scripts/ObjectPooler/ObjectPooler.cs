﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Object Pool based on Brackeys Video https://www.youtube.com/watch?v=tdSmKaJvCoA
public class ObjectPooler : MonoBehaviour
{

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int count;
        //public Transform destination;
        private GameObject poolHolder;
    }

    public static ObjectPooler Instance;
    public List<Pool> m_pools;
    public Dictionary<string, Queue<GameObject>> m_poolDictionary;
    //public Transform baseHolder;

    private void Awake()
    {
        Instance = this;
        m_poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool p in m_pools)
        {
            //object holder
            GameObject poolHolder = new GameObject(p.tag + "s");
            poolHolder.transform.parent = transform;
            poolHolder.transform.position = Vector3.zero;
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < p.count; i++)
            {
                GameObject obj = Instantiate(p.prefab, poolHolder.transform);
                //Start/Awake replacement
                iPooledObject pooledObject = obj.GetComponent<iPooledObject>();
                if (pooledObject != null)
                {
                    pooledObject.OnObjectInstantiation();
                }

                obj.name = p.tag + "(" + (i + 1) + ")";
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            m_poolDictionary.Add(p.tag, objectPool);
        }
    }


    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!m_poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + " Doesn't exist.");
            return null;
        }

        //Set Object
        GameObject objectToSpawn = m_poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        //Requeue object to dictionary to reuse
        m_poolDictionary[tag].Enqueue(objectToSpawn);

        //Call On Spawn Function
        iPooledObject pooledObject = objectToSpawn.GetComponent<iPooledObject>();

        if (pooledObject != null)
        {
            pooledObject.OnObjectSpawn();
        }
        return objectToSpawn;
    }

    public void ResetAllPools()
    {
        foreach(Pool p in m_pools)
        {
            ResetPool(p.tag);
        }
    }

    private void ResetPool(string tag)
    {
        if (m_poolDictionary.ContainsKey(tag))
        {
            foreach(GameObject go in m_poolDictionary[tag])
            {
                go.SetActive(false);
            }
        }
    }
}


