﻿using UnityEngine;

public interface iPooledObject
{
    void OnObjectSpawn();
    void OnObjectInstantiation();
}
